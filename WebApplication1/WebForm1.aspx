﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Projekt w ASP.NET</title>
</head>
<body>
    <h1>Projekt w ASP.NET</h1>
    <form id="form1" runat="server">
        <div>
            <label>Imię i nazwisko</label>
            <input runat="server" id="NameInput" />
            <button type="submit" runat="server" onServerClick="OnSaveClick">Zapisz</button>
        </div>
        
        <div>
            <h3>Twoje dane: <%= NameValue %></h3>
            <button type="button" runat="server" onServerClick="OnSwapClick">Zamień kolejność</button>
            <span style="color: red;"><%= ErrorValue %></span>
        </div>
    </form>
</body>
</html>

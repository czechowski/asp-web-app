﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        public string NameValue = "";
        public string ErrorValue = "";

        protected void OnSaveClick(object sender, EventArgs e)
        {
            NameValue = NameInput.Value;
        }

        protected void OnSwapClick(object sender, EventArgs e)
        {
            string[] parts = NameValue.Split(' ');
            if (parts.Length > 1)
            {
                NameValue = parts[1] + " " + parts[0];
                ErrorValue = "";
            }
            else if (NameValue.Length == 0)
            {
                ErrorValue = "Wprowadź dane i naciśnij \"Zapisz\"";
            }
            else
            {
                ErrorValue = "Imię i nazwisko muszą być oddzielone spacją";
            }
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            NameValue = (string)ViewState["NameValue"];
            ErrorValue = (string)ViewState["ErrorValue"];
        }

        protected override object SaveViewState()
        {
            ViewState["NameValue"] = NameValue;
            ViewState["ErrorValue"] = ErrorValue;
            return base.SaveViewState();
        }
    }
}